/**
* Video
*
* @author rkalms <rkalms@vertic.com>
*/

define(['jquery'], function ($) {
	var defaults = {};

	function Video(el, options) {
		var self = this;

		this.options = $.extend({}, defaults, options);

		// Globals
		this.$el = $(el);

		this.canvas = $('#video-piger').get(0);
		this.ctx = this.canvas.getContext('2d');
		this.video = $('[data-video]').get(0);
		this.vidTimer = null;

		var vidSet = function () {
			clearTimeout(self.vidTimer);
			self.vidTimer = setTimeout(showVid, 25);
			console.log('ping vidSet');
		};

		var showVid = function () {
			self.ctx.drawImage(self.video, 0, 0);
			self.processImage();

			if (!self.video.paused)
				// Repeat 40 times a second to oversample 30 fps videoøøø
				self.vidTimer = setTimeout(showVid, 25);
		};

		this.video.addEventListener('play', vidSet(), false);

		this.init();
	}

	Video.prototype.processImage = function () {
		console.log('processImage()');

		// get pixel data for entire canvas
		pixels = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
		pixData = pixels.data;

		// for each pixel
		// for (i = 0; i < this.can.width * this.can.height; i++) {
		// 	// get combined rgb value to determine brightness
		// 	var rgbVal = pixData[i*4] + pixData[i*4 + 1] + pixData[i*4 + 2];
		// 	// set alpha value for dark pixels to knob value
		// 	if (rgbVal < 150)
		// 		pixData[i*4 + 3] = alphaVal;
		// }

		 // put modified data back into image object
		 pixels.data = pixData;

		 // blit modified image object to screen
		 this.ctx.putImageData(pixels, 0, 0);
	};

	Video.prototype.play = function (video) {
		video.play();
	};	

	Video.prototype.init = function () {
		this.ctx.drawImage(this.video, 0, 0);

		this.processImage();

		this.$el.on('play', function () {
			this.play(this.video);
		}.bind(this))

		// this.$el.on('click', '.button', function (e) {
		// 	this.play(this.video);

		// 	e.preventDefault();
		// }.bind(this));

		//;

		window.document.body.onload = this.play(this.video); 
	};

	return Video;
});
