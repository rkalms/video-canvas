/*
 * Vertic JS - Site functional wrapper
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 *
 */

/*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true, Modernizr: true*/

/* Remember to update Gruntfile.js with config */
require.config({
	paths: {
		'jquery': '../components/jquery/jquery',
	}
});

require(['framework/core', 'jquery', 'project/video'], function(core, $, Video) {
	'use strict';

	// Expose core as vertic first for debugging reasons
	window.vertic = core;

	// Document ready
	$(function () {
		//
		var video = new Video($('#animation-frame'), {});
	});
});
