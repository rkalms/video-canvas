'use strict';

var express = require('express'),
	fs = require('fs');

var app = express();

// Routes
app.use(express.static('app'));

app.get(/\.(mjpeg)$/i, function(request, res) {
	var path = res.req._parsedOriginalUrl.path,
		string = path.replace(/.*\\|\..*$/g, ''),
		name = string.replace('/', '');

	var files = fs.readdirSync('app/assets/streams/' + name + '/');

	res.writeHead(200, {
		'Content-Type': 'multipart/x-mixed-replace; boundary=myboundary',
		'Cache-Control': 'no-cache',
		'Connection': 'close',
		'Pragma': 'no-cache'
	});

	var i = 0;
	var stop = false;

	console.log(files.length);

	res.connection.on('close', function() { stop = true; });

	var send_next = function () {
		if (stop)
			return;

		i = (i+1) % 300;

		var filename = i + ".jpg";

		fs.readFile('app/assets/streams/' + name + '/' + filename, function (err, content) {
			res.write("--myboundary\r\n");
			res.write("Content-Type: image/jpeg\r\n");
			res.write("Content-Length: " + content.length + "\r\n");
			res.write("\r\n");
			res.write(content, 'binary');
			res.write("\r\n");

			// Start next in stream
			setTimeout(send_next, 42);
		});
	};

	send_next();
});

// Setup
var port = process.env.PORT || 5000;

app.listen(port, function() {
    console.log('Listening on ' + port);
});
